# Shopping List

the main purpose of this app is to
click all the items i need for my shopping list.
Then print it easily with a different view.
And work html css JS.

## Some links

**The @media print**    
[a tutorial on @media print / screen](https://www.willmaster.com/library/features/a_printer-friendly_web_page_with_non-printing_elements.php)    
[the MDN doc](https://developer.mozilla.org/fr/docs/Web/CSS/@media)    

**Classlist**    
[add remove css classes using JS](https://appendto.com/2016/02/addremove-css-classes-using-javascript/)

## Various notes about the application
+ make categories
+ the item with qty + -
+ select / deselect All
+ Customize the print version (text size)


## model

Title

category_1
item1 qty plusBt minusBt
item2 qty plusBt minusBt
item3 qty plusBt minusBt

category_2
item1 qty plusBt minusBt
item2 qty plusBt minusBt
item3 qty plusBt minusBt


## Main versions history

/******************************************************/     
Version 0.1 on master 02/01/2017 :
A fully basic version with:
- Create sections and items in the list.js
- automaticly create the template with cat and articles
- select / unselect articles
- Add / sub numbers of articles
- when print create the list with the selected articles.