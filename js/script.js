console.log("script say hi!");

/* Create the items */

console.log(shopList);


function createSection(sectionName, itemList) {
  	const newSection = document.createElement("fieldset");
	let makeItems = `<legend><h2>${sectionName}</h2></legend>`;

	for (el in itemList) {
		makeItems += addItem(itemList[el]);
		itNum++;
	}

	newSection.innerHTML = makeItems;
	document.body.appendChild(newSection);
}


function addItem(itemName) {
	return `<div class="itemToSelect noprint">\
			<input type="checkbox" id="item_${itNum}" name="cat${itNum}" value="item ${itNum}">\
			<div class="checkForPrint"></div>\
			<label for="cat${itNum}">${itemName}</label>\
			<p class="quantity">1</p>\
			<button class="plusBt">+</button>\
			<button class="minusBt">-</button>\
		</div>`;
}


let itNum = 0;

for (let prop in shopList) {
  console.log(`obj.${prop} = ${shopList[prop]}`);
  createSection(prop, shopList[prop]);
}


/* Set the interaction */

let items = document.querySelectorAll("input");


items.forEach(function(checkBox){
	checkBox.checked = false;
});


function addEventListenerList(list, event, fn) {
    for (var i = 0, len = list.length; i < len; i++) {
        list[i].addEventListener(event, fn, false);
    }
}


function getItemState (item) {

	if (item.checked) {
		return true;
	} else {
		return false;
	}
}


function displayItem() {
   	let selectItem = document.querySelector('#' + this.id);
    let toChange = selectItem.parentElement.classList;

    if (getItemState(this) && toChange.contains("noprint")) {
		toChange.remove("noprint");
	} else if (!getItemState(this)) {
		toChange.add("noprint");
	}
}


addEventListenerList(items, 'click', displayItem);



/* PLUS BUTTON */

let plusBt = document.querySelectorAll('.plusBt');

function incrementQuantity() {
	console.log(this);
	let qtyValue = Number(this.previousElementSibling.innerHTML);
	this.previousElementSibling.innerHTML = qtyValue + 1;
}

addEventListenerList(plusBt, 'click', incrementQuantity);



/* MINUS BUTTON */

let minusBt = document.querySelectorAll('.minusBt');

function decrementQuantity() {
	console.log(this);
	let qtyValue = Number(this.previousElementSibling.previousElementSibling.innerHTML); 
	if (qtyValue > 1) {
		this.previousElementSibling.previousElementSibling.innerHTML = qtyValue - 1;
	}
	
}

addEventListenerList(minusBt, 'click', decrementQuantity);